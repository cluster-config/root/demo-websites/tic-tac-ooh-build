FROM alpine:3.11.3 AS BASE
RUN apk update && apk upgrade && apk add npm

FROM BASE AS BUILD
ADD . /build
WORKDIR /build
RUN npm i -g @angular/cli && npm i && ng build --prod

FROM BASE
COPY --from=BUILD /build/dist /build/dist
RUN npm i serve -g
EXPOSE 5000
CMD serve /build/dist/angular-frontend
