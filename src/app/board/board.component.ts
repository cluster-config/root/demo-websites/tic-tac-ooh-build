import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { PlayerType } from './playerType';

// Written with https://www.youtube.com/watch?v=G0bBLvWXBvc&ab_channel=Fireship as a base.
// Themed with: https://akveo.github.io/nebular/docs/design-system/eva-design-system-intro#implementations

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
  squares!: PlayerType[][];
  xMovesNext!: boolean;
  winner!: PlayerType;
  boardLength: number;
  numInRowToWin: number;

  constructor() {
    this.boardLength = 3;
    this.numInRowToWin = 3;
  }

  setBoardLength(value: number): void {
    this.boardLength = value;
    this.numInRowToWin = Math.min(this.boardLength, this.numInRowToWin);
    this.newGame();
  }

  setNumInRowToWin(value: number): void {
    this.numInRowToWin = value;
  }

  ngOnInit(): void {
    this.newGame();
  }

  newGame(): void {
    this.squares = [];
    for (let i = 0; i < this.boardLength; i++) {
      this.squares[i] = [];
      for (let k = 0; k < this.boardLength; k++) {
        this.squares[i][k] = '';
      }
    }
    this.winner = '';
    this.xMovesNext = true;
  }

  playerAtPos(row: number, column: number): PlayerType {
    return this.squares[row][column];
  }

  get size(): number {
    return 500 / this.boardLength;
  }

  get player(): PlayerType {
    return this.xMovesNext ? 'X' : 'O';
  }
  get lastPlayer(): PlayerType {
    return !this.xMovesNext ? 'X' : 'O';
  }

  makeMove(row: number, column: number): void {
    if (!this.squares[row][column] && !this.winner) {
      this.squares[row][column] = this.player;
      this.winner = this.calculateWinner(row, column);
      this.xMovesNext = !this.xMovesNext;
    }
  }

  calculateWinner(row: number, column: number): PlayerType {
    const directions: number[][] = [
      [1, 0],
      [0, 1],
      [1, 1],
      [1, -1],
    ];
    for (const direction of directions) {
      let numFoundInRow = this.checkSquares(row, column, direction, 1);
      numFoundInRow += this.checkSquares(row, column, direction, -1);
      if (numFoundInRow + 1 >= this.numInRowToWin) {
        return this.player;
      }
    }
    return '';
  }

  checkSquares(
    row: number,
    column: number,
    direction: number[],
    reverse: number
  ): number {
    let numFoundInRow = 0;
    for (let i = 1; i < this.numInRowToWin; i++) {
      let row_temp = row + direction[0] * i * reverse;
      let column_temp = column + direction[1] * i * reverse;
      if (
        row_temp >= 0 &&
        row_temp < this.boardLength &&
        column_temp >= 0 &&
        column_temp < this.boardLength &&
        this.squares[row_temp][column_temp] === this.player
      ) {
        numFoundInRow++;
      } else {
        break;
      }
    }
    return numFoundInRow;
  }

  trackByFn(item: any, id: any): any {
    return item;
  }
}
