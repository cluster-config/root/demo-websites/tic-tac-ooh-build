import { Component, Input } from '@angular/core';
import { PlayerType } from '../board/playerType';

@Component({
  selector: 'app-square',
  template: `
    <button
      nbButton
      hero
      [attr.aria-label]="label"
      [status]="classes"
      [style.height.px]="this.size"
      [style.width.px]="this.size"
      [style.font-size.px]="this.size / 2"
    >
      {{ player }}
    </button>
  `,
  styles: ['button{margin:0.25rem}'],
})
export class SquareComponent {
  @Input() player!: PlayerType;
  @Input() size!: number;
  @Input() label!: string;

  get classes() {
    if (this.player === 'X') return 'danger';
    else if (this.player === 'O') return 'success';
    else return '';
  }
}
